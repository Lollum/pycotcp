import sys
import pycotcp

import sys, getopt

helpExample = "socketClient.py -D vde,pic0,/tmp/pic0.ctl -L 10.40.0.50 --netmask 255.255.255.0 -o socket0,ipv4,udp -b socket0,10.40.0.50,6667 -c socket0,10.40.0.55,6667 -s socket0,'Message',100"
helpVerbose = "-h --help | This help\n"
helpVerbose += "-L --localaddr | Address of the device: -L 10.50.0.50\n"
helpVerbose += "-M --netmask | Netmask of the device: -M 255.255.255.0\n"
helpVerbose += "-L --device | Device |name,type,path| if needed: -D vde,pic0,/tmp/pic0.ctl\n"

helpVerbose += "-o --open | Open a socket |socket,ip_type,protocol|: -o socket0,ipv4,udp\n"
helpVerbose += "-b --bind | Bind a socket |socket,local_address,local_port|: -b socket0,10.40.0.50,6667\n"
helpVerbose += "-c --connect |[optional] Connect to a socket |socket,remote_address,remote_port|: -c socket0,10.40.0.55,6667\n"
helpVerbose += "-s --send |[multiple] Send a message through the socket |socket,message,message_len|: -s socket0,'A message',100\n"
helpVerbose += "-w --write |[multiple] Write a message through the socket |socket,message,message_len|: -w socket0,'Some message',100\n"
helpVerbose += "-S --sendto |[multiple] Send a message to the address |socket,message,message_len,remote_address,remote_port|: -S socket0,'Another message',100:10.40.0.55,6667\n"
helpVerbose += "-e --sendtoext |[multiple] Same as SendTo, but can specify TTL e TOS |socket,message:message_len,remote_address,remote_port,TTL,TOS|: -e socket0,'A third message,100,10.40.0.55,6667,30,0"

commType = "ipv4"
separator = ","

sendList = []
sendToList = []
sendToExtList = []
writeList = []
closeConn = False
pckDim = 512

def callback_socket_udp(param,socket):
    global commType
    global closeConn
    global pckDim
    if (param == "RD"):
        recv = pycotcp.socketRecvFromExt(socket,pckDim,commType)
        if (recv[0] == "END"):
            closeConn = True
        else:
            print "socketRecvFromExt: " + str(recv)
    else:
        print "socketEcho: "+str(param)

def callback_socket_tcp(param,socket):
    global sendList
    global sendToList
    global sendToExtList
    global writeList
    global closeConn
    global pckDim
    if (param == "CONN"):
        print "Connesso al server"
    elif (param == "RD"):
        res = pycotcp.socketRead(socket,pckDim)
        print "socketRead:"+str(res)
    elif(param == "WR"):
        for sendDo in sendList:
            err = pycotcp.socketSend(sendDo[0],sendDo[1],int(sendDo[2]))
            print "socketSend: "+ str(err)
            if err < 0:
                print pycotcp.getPicoError()
                sys.exit(2)
        sendList=[]
        for sendDo in sendToList:
            err = pycotcp.socketSendTo(sendDo[0],sendDo[1],int(sendDo[2]),sendDo[3],sendDo[4])
            print "socketSendTo: "+ str(err)
            if err < 0:
                print pycotcp.getPicoError()
                sys.exit(2)
        sendToList = []
        for sendDo in sendToExtList:
            err = pycotcp.socketSendToExt(sendDo[0],sendDo[1],int(sendDo[2]),sendDo[3],sendDo[4],int(sendDo[5]),int(sendDo[6]))
            print "socketSendToExt: "+ str(err)
            if err < 0:
                print pycotcp.getPicoError()
                sys.exit(2)
        sendToExtList = []
        for sendDo in writeList:
            err = pycotcp.socketWrite(sendDo[0],sendDo[1],int(sendDo[2]))
            print "socketWrite: "+ str(err)
            if err < 0:
                print pycotcp.getPicoError()
                sys.exit(2)
        writeList = []
        print "socketShutdown: "+ str(pycotcp.socketShutdown(socket,"w"))
    elif(param == "CLOSE"):
        print "Sever closed connection"
        closeConn = True
    elif(param == "FIN"):
        print "Socket closed"
        closeConn = True

def main(argv):
    devtypeList = ["vde","tap","tun"]
    address = netmask = devName = devType = devPath = ""
    protocolO = versionO = sockNameO = localportB = sockNameB = addrB = ""
    socketC = addrRC = portRC = ""

    global commType
    
    try:
        opts, args = getopt.getopt(argv,"hL:M:D:o:b:s:S:e:c:w:",["help","localaddr=","netmask=","device=","open=","bind=","send=","sendto=","sendtoext=","connect=","write="])
    except getopt.GetoptError:
        print "How to use: "+helpExample
        sys.exit(2)
    for opt, arg in opts:
        if opt in ("-h", "--help"):
            print helpVerbose
            print "Example: "+helpExample
            sys.exit()
        elif opt in ("-L", "--localaddr"):
            address = arg
        elif opt in ("-M", "--netmask"):
            netmask = arg
        elif opt in ("-D", "--device"):
            devArg = arg.split(separator)
            if (devArg[0] in devtypeList):
                devType = devArg[0]
                if len(devArg) == 1:
                    pass
                elif len(devArg) == 2:
                    devName = devArg[1]
                elif len(devArg) == 3:
                    devName = devArg[1]
                    devPath = devArg[2]
                else:
                    print "Error device"
                    sys.exit(2)
            else:
                print "Error device"
                sys.exit(2)
#######################################################################        
        elif opt in ("-o", "--open"):
            newArg = arg.split(separator)
            print newArg
            if len(newArg) != 3:
                print "Error open" + str(newArg)
                sys.exit(2)
            sockNameO = newArg[0]
            commType = versionO = newArg[1]
            protocolO = newArg[2]
        
        elif opt in ("-b", "--bind"):
            newArg = arg.split(separator)
            if len(newArg) != 3:
                print "Error bind" + str(newArg)
                sys.exit(2)
            sockNameB = newArg[0]
            addrB = newArg[1]
            localportB = newArg[2]
            
        elif opt in ("-s", "--send"):
            newArg = arg.split(separator)
            if len(newArg) != 3:
                print "Error send" + str(newArg)
                sys.exit(2)
            sendList.append(newArg)
        
        elif opt in ("-S", "--sendto"):
            newArg = arg.split(separator)
            if len(newArg) != 5:
                print "Error sendto" + str(newArg)
                sys.exit(2)
            sendToList.append(newArg)
        
        elif opt in ("-e", "--sendtoext"):
            newArg = arg.split(separator)
            if len(newArg) != 7:
                print "Error sendtoext" + str(newArg)
                sys.exit(2)
            sendToExtList.append(newArg)
            
        elif opt in ("-w", "--write"):
            newArg = arg.split(separator)
            if len(newArg) != 3:
                print "Error write" + str(newArg)
                sys.exit(2)
            writeList.append(newArg)
            
        elif opt in ("-c", "--connect"):
            newArg = arg.split(separator)
            if len(newArg) != 3:
                print "Error connect" + str(newArg)
                sys.exit(2)
            socketC = newArg[0]
            addrRC = newArg[1]
            portRC = newArg[2]
                   
        else:
            print "Error arguments"
            sys.exit(2)
    if address == "" or netmask == "" or devName == "":
        print "How to use: "+helpExample
        sys.exit(2)
#######################################################################  
    
    pycotcp.init()
    if devName != "":
        if devPath != "":
            print "createDevice: "+str(pycotcp.createDevice(devType,devName,devPath))
        else:
           print "createDevice: "+str(pycotcp.createDevice(devType,devName)) 
    else:
        print "createDevice: "+str(pycotcp.createDevice(devType))
    print "linkAddIp4: "+ str(pycotcp.linkAddIp4(devName,address,netmask))
    if (protocolO  == ""  or  versionO  == ""  or sockNameO  == "" ):
        print "no open arguments"
        sys.exit(2)        
    if (protocolO == "udp"):
        print "socketOpenUDP: "+ str(pycotcp.socketOpen(sockNameO,versionO,protocolO,callback_socket_udp))
    elif (protocolO == "tcp"):
        print "socketOpenTCP: "+ str(pycotcp.socketOpen(sockNameO,versionO,protocolO,callback_socket_tcp))
    else:
        print "Wrong protocol"
        sys.exit(2) 
    if (localportB == "" or  sockNameB == "" or addrB  == "" ):
        print "no bind arguments"
        sys.exit(2)
    print "socketBind: "+ str(pycotcp.socketBind(sockNameB,addrB,localportB));
    if (socketC != "" or  addrRC != "" or portRC != ""):
        print "socketConnect: "+ str(pycotcp.socketConnect(socketC,addrRC,portRC));

    for sendDo in sendList:
        err = pycotcp.socketSend(sendDo[0],sendDo[1],int(sendDo[2]))
        print "socketSend: "+ str(err)
        if err < 0:
            print pycotcp.getPicoError()
            sys.exit(2)
            
    for sendDo in sendToList:
        err = pycotcp.socketSendTo(sendDo[0],sendDo[1],int(sendDo[2]),sendDo[3],sendDo[4])
        print "socketSendTo: "+ str(err)
        if err < 0:
            print pycotcp.getPicoError()
            sys.exit(2)
            
    for sendDo in sendToExtList:
        err = pycotcp.socketSendToExt(sendDo[0],sendDo[1],int(sendDo[2]),sendDo[3],sendDo[4],int(sendDo[5]),int(sendDo[6]))
        print "socketSendToExt: "+ str(err)
        if err < 0:
            print pycotcp.getPicoError()
            sys.exit(2)
            
    for sendDo in writeList:
        err = pycotcp.socketWrite(sendDo[0],sendDo[1],int(sendDo[2]))
        print "socketWrite: "+ str(err)
        if err < 0:
            print pycotcp.getPicoError()
            sys.exit(2)
    if protocolO == "udp":
        err = pycotcp.socketSend(sendDo[0],"END",4)
        print "socketSend Signal: "+ str(err)
    try:
        while not closeConn:
            pycotcp.stackTick()
            pycotcp.idle()
        sys.exit()
    except KeyboardInterrupt:
        sys.exit()
    
if __name__ == "__main__":
   main(sys.argv[1:])
