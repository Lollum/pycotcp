import sys
import pycotcp

import sys, getopt

countpck = 0
separator = ","

helpExample = "ping.py -D vde,pic0,/tmp/pic0.ctl -L 10.40.0.10 -M 255.255.255.0 -d 10.40.0.1"
helpVerbose = "-h --help | This help\n"
helpVerbose += "-L --localaddr | Address of the device: -L 10.50.0.11\n"
helpVerbose += "-M --netmask | Netmask of the device: -M 255.255.255.0\n"
helpVerbose += "-L --device | Device name:type:path if needed: -D vde,pic0,/tmp/pic0.ctl\n"
helpVerbose += "-d --dest | Destination address: -d 10.50.0.1 \n"
helpVerbose += "-n --number | [optional] Number of ping to send: -n 10 \n"
helpVerbose += "-i --interval | [optional] Interval of time between pings, in milliseconds: -i 1000 \n"
helpVerbose += "-t --timeout | [optional] Time to elapse before considering timeout, in milliseconds: -t 3000\n"
helpVerbose += "-p --pktsize | [optional] Size of single packets: -p 64"

def callback_ping(dest,size,seq,time,ttl,err):
    global countpck
    countpck = seq
    if (err == 0):
        print "recv: "+str(dest)+" size:"+str(size)+" seq:"+str(seq)+" time:"+str(time)+" ttl:"+str(ttl)+" err:"+str(err)
    else:
        print "error: "+str(dest)+" seq:"+str(seq)+" err:"+str(err) 

def main(argv):
    devtypeList = ["vde","tap","tun"]
    address = netmask = devName = devType = devPath = dest = ""
    number = 10
    interval = 1000
    timeout = 3000
    packsize = 64
    try:
        opts, args = getopt.getopt(argv,"hL:M:D:d:n:i:t:p:",["help","localaddr=","netmask=","device=","dest=","number=","interval=","timeout=","pktsize="])
    except getopt.GetoptError:
        print "How to use: "+helpExample
        sys.exit(2)
    for opt, arg in opts:
        if opt in ("-h", "--help"):
            print helpVerbose
            print "Example: "+helpExample
            sys.exit()
        elif opt in ("-L", "--localaddr"):
            address = arg
        elif opt in ("-M", "--netmask"):
            netmask = arg
        elif opt in ("-D", "--device"):
            devArg = arg.split(separator)
            if (devArg[0] in devtypeList):
                devType = devArg[0]
                if len(devArg) == 1:
                    pass
                elif len(devArg) == 2:
                    devName = devArg[1]
                elif len(devArg) == 3:
                    devName = devArg[1]
                    devPath = devArg[2]
                else:
                    print "Error device"
                    sys.exit(2)
            else:
                print "Error device"
                sys.exit(2)
        elif opt in ("-d", "--dest"):
            dest = arg
        elif opt in ("-n", "--number"):
            number = int(arg)
        elif opt in ("-i", "--interval"):
            interval = int(arg)
        elif opt in ("-t", "--timeout"):
            timeout = int(arg)
        elif opt in ("-p", "--pktsize"):
            packsize = int(arg)
        else:
            print "Error arguments"
            sys.exit(2)
    if address == "" or netmask == "" or devName == "" or dest == "":
        print "How to use: "+helpExample
        sys.exit(2)
    pycotcp.init()
    if devName != "":
        if devPath != "":
            print "createDevice: "+str(pycotcp.createDevice(devType,devName,devPath))
        else:
           print "createDevice: "+str(pycotcp.createDevice(devType,devName)) 
    else:
        print "createDevice: "+str(pycotcp.createDevice(devType))
    print "linkAddIp4: "+str(pycotcp.linkAddIp4(devName,address,netmask))
    idping = pycotcp.pingStartIp4(dest,number,interval,timeout,packsize,callback_ping)
    print "pingStartIp4: "+str(idping)
    try:
        while countpck < number:
            pycotcp.stackTick()
            pycotcp.idle()
        print "pingAbortIp4: "+str(pycotcp.pingAbortIp4(idping))
        sys.exit()
    except KeyboardInterrupt:
        sys.exit()
    
if __name__ == "__main__":
   main(sys.argv[1:])
   
