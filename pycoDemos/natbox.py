import sys
import pycotcp

import sys, getopt

countpck = 0
separator = ","

helpExample = "\nnatbox.py -D vde,pic0,/tmp/pic0.ctl -L 10.40.0.50 --netmask 255.255.255.0 -r 10.0.40.0.50,5555,10.40.0.55,6667,udp,add -r 10.0.40.50,5556,10.40.0.52,6667,udp,add"
helpExample += "\nsocketEcho.py -D vde,pic0,/tmp/pic0.ctl -L 10.40.0.55 --netmask 255.255.255.0 -o socket0,ipv4,udp -b socket0,10.40.0.55,6667"
helpExample += "\nsocketClient.py -D vde,pic0,/tmp/pic0.ctl -L 10.40.0.52 --netmask 255.255.255.0 -o socket0,ipv4,udp -b socket0,10.40.0.52,5555 -c socket0,10.40.0.50,5555 -s socket0,'Message',100"

helpVerbose = "-h --help | This help\n"
helpVerbose += "-L --localaddr | Address of the device: -L 10.50.0.50\n"
helpVerbose += "-M --netmask | Netmask of the device: -M 255.255.255.0\n"
helpVerbose += "-L --device | Device |name:type:path| if needed: -D vde,pic0,/tmp/pic0.ctl\n"
helpVerbose += "-r --rule | [multiple]  Rule to add to the NAT, can be called multiple times |public_addr,public_port,private_addr,private_port,protocol,persistent| : -r 10.0.40.0.50:5555,10.40.0.55,6667,udp,add"

def main(argv):
    devtypeList = ["vde","tap","tun"]
    address = netmask = devName = devType = devPath = ""
    link = ""
    ruleList = []
    try:
        opts, args = getopt.getopt(argv,"hL:M:D:r:",["help","localaddr=","netmask=","device=","rule="])
    except getopt.GetoptError:
        print "How to use: "+helpExample
        sys.exit(2)
    for opt, arg in opts:
        if opt in ("-h", "--help"):
            print helpVerbose
            print "Example: "+helpExample
            sys.exit()
        elif opt in ("-L", "--localaddr"):
            address = arg
        elif opt in ("-M", "--netmask"):
            netmask = arg
        elif opt in ("-D", "--device"):
            devArg = arg.split(separator)
            if (devArg[0] in devtypeList):
                devType = devArg[0]
                if len(devArg) == 1:
                    pass
                elif len(devArg) == 2:
                    devName = devArg[1]
                elif len(devArg) == 3:
                    devName = devArg[1]
                    devPath = devArg[2]
                else:
                    print "Error device"
                    sys.exit(2)
            else:
                print "Error device"
                sys.exit(2)
        elif opt in ("-r", "--rule"):
            newrule = arg.split(separator)
            if len(newrule) != 6:
                print "Error rule" + str(newrule)
                sys.exit(2)
            ruleList.append(newrule)
        else:
            print "Error arguments"
            sys.exit(2)
    if address == "" or netmask == "" or devName == "":
        print "How to use: "+helpExample
        sys.exit(2)
        
        

    pycotcp.init()
    if devName != "":
        if devPath != "":
            print "createDevice: "+str(pycotcp.createDevice(devType,devName,devPath))
        else:
           print "createDevice: "+str(pycotcp.createDevice(devType,devName)) 
    else:
        print "createDevice: "+str(pycotcp.createDevice(devType))
    print "linkAddIp4: "+ str(pycotcp.linkAddIp4(devName,address,netmask))
    link = pycotcp.linkGetIp4("link0",address)
    print "linkGetIp4: "+ str(link)
    print "natEnableIp4: "+ str(pycotcp.natEnableIp4(link))
    for rule in ruleList:
        pubAddr = rule[0]
        pubPort = rule[1]
        prvPort = rule[3]
        prvAddr = rule[2]
        proto = rule[4]
        pers = rule[5]
        err = pycotcp.portForwardIp4(pubAddr,pubPort,prvAddr,prvPort,proto,pers)
        print "portForwardIp4: "+ str(err)
        if err < 0:
            print pycotcp.getPicoError()
    try:
        while True:
            pycotcp.stackTick()
            pycotcp.idle()
        sys.exit()
    except KeyboardInterrupt:
        sys.exit()
    
if __name__ == "__main__":
   main(sys.argv[1:])
