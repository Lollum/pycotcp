import sys
import pycotcp
import time
import getopt
import os
import io
ended = False
separator = ","
ipType = ""

helpExample = "tftpClient.py -D vde,pic0,/tmp/pic0.ctl -L 10.40.0.3 --netmask 255.255.255.0 -t ipv4 -T 10.40.0.2,DemoFile/Source/canto21.txt,rx -f DemoFile/Result/12otnac.txt"
helpVerbose = "######## WARNING: Due how Python handle files and String, this demo is designed with text file in mind #######\n"
helpVerbose += "-h --help | This help\n"
helpVerbose += "-L --localaddr | Address of the device: -L 10.50.0.50\n"
helpVerbose += "-M --netmask | Netmask of the device: -M 255.255.255.0\n"
helpVerbose += "-D --device | Device |name:type:path| if needed: -D vde,pic0,/tmp/pic0.ctl\n"
helpVerbose += "-t --type | Protocol to use: -t ipv4\n"
helpVerbose += "-T --transfert | Set up the transfert with the remote server, the name of the file and type rx/tx: -T 10.40.0.2,DemoFile/Source/canto21.txt,rx\n"
helpVerbose += "-f --filename | Filename used when receive: -f DemoFile/Result/CantoVentunoBis.txt\n"
helpVerbose += "-e --encoding | Encoding of the file, default utf-8: -e utf-8"

filetransfert = 0
filename = ""
encoding = "utf-8"

def callback_tftptx(session,event,block,leng,note):
    global ended
    if event == pycotcp.TFTP_EV_ERR_PEER:
        print "Remote Error"
        ended = True
    elif event == pycotcp.TFTP_EV_OK:
        send = pycotcp.tftpSend(session,note)
        print "Send block: "+str(send)
        if (send[0] < 512):
            print "[TX] Transfer Complete"
            ended = True
    else:
        print "Event receive: "+str(event)

def callback_tftprx(session,event,block,leng,note):
    global filetransfert
    global filename
    global ended
    global encoding
    if event == pycotcp.TFTP_EV_ERR_PEER:
        print "Remote Error"
        ended = True
    if event == pycotcp.TFTP_EV_OK:
        if filetransfert == 0:
            f = io.open(filename, 'w')
        else:
            f = io.open(filename, 'a')
        filetransfert += leng
        print "Recv: "+str(filetransfert)
        try:
            towrite = block[:leng].decode(encoding) ##stringa sporca
            f.write(towrite)
        except Exception,e:
            print str(e)
        if leng < 512:
            print "[RX] Transfer Complete"        
            ended = True
    else:
        print "Event receive: "+str(event)

def main(argv):
    global ipType
    global filetransfert
    global filename
    global encoding
    devtypeList = ["vde","tap","tun"]
    address = netmask = devName = devType = devPath = ""
    dnsList = []
    serverAddr = filenameServer = ""
    transferType = ""
    try:
        opts, args = getopt.getopt(argv,"hL:M:D:t:f:e:T:",["help","localaddr=","netmask=","device=","type=","filename=","encoding=","transfer="])
    except getopt.GetoptError:
        print "How to use: "+helpExample
        sys.exit(2)
    for opt, arg in opts:
        if opt in ("-h", "--help"):
            print helpVerbose
            print "Example: "+helpExample
            sys.exit()
        elif opt in ("-L", "--localaddr"):
            address = arg
        elif opt in ("-M", "--netmask"):
            netmask = arg
        elif opt in ("-D", "--device"):
            devArg = arg.split(separator)
            if (devArg[0] in devtypeList):
                devType = devArg[0]
                if len(devArg) == 1:
                    pass
                elif len(devArg) == 2:
                    devName = devArg[1]
                elif len(devArg) == 3:
                    devName = devArg[1]
                    devPath = devArg[2]
                else:
                    print "Error device"
                    sys.exit(2)
            else:
                print "Error device"
                sys.exit(2)
########################################################################        
        elif opt in ("-t", "--type"):
            ipType = arg
        elif opt in ("-e", "--encoding"):
            encoding = arg
        elif opt in ("-f", "--filename"):
            filename = arg
            filetransfert = 0
        elif opt in ("-T", "--transfer"):
            newArg = arg.split(separator)
            print newArg
            if len(newArg) != 3:
                print "Error open" + str(newArg)
                sys.exit(2)
            serverAddr = newArg[0]
            filenameServer = newArg[1]
            transferType = newArg[2]
        else:
            print "Error arguments"
            sys.exit(2)
########################################################################
    if address == "" or netmask == "" or devName == "" or ipType == "" or serverAddr == "":
        print "How to use: "+helpExample
        sys.exit(2)        
        
    pycotcp.init()
    if devName != "":
        if devPath != "":
            print "createDevice: "+str(pycotcp.createDevice(devType,devName,devPath))
        else:
           print "createDevice: "+str(pycotcp.createDevice(devType,devName)) 
    else:
        print "createDevice: "+str(pycotcp.createDevice(devType))
    print "linkAddIp4: "+ str(pycotcp.linkAddIp4(devName,address,netmask))
    result = (pycotcp.tftpSessionSetup(ipType,filenameServer,serverAddr))
    print "tftpSessionSetup: "+str(result)
    if transferType == "rx":
        print "tftpStartRx: "+str(pycotcp.tftpStartRx(pycotcp.TFTP_PORT,filenameServer,callback_tftprx,result[0],result[1]))  
    elif transferType == "tx":
        print "tftpStartTx: "+str(pycotcp.tftpStartTx(pycotcp.TFTP_PORT,filename,callback_tftptx,result[0],result[1]))
    else:
        print "Unknown transfer type"
        sys.exit(2)
    try:
        while not ended:
            pycotcp.stackTick()
            pycotcp.idle()
        sys.exit()
    except KeyboardInterrupt:
        sys.exit()
    
if __name__ == "__main__":
   main(sys.argv[1:])
