import sys
import pycotcp
import time
import sys, getopt

ended = False
separator = ","
serviceList = []
vectorList = []
kvList = []

helpExample = "dnssd.py -D vde,pic0,/tmp/pic0.ctl -L 10.40.0.55 --netmask 255.255.255.0 -g 10.40.0.1 -i fubar.local,10.40.0.55 -s WebServer,_http._tcp,80,vector0,30 -k vector0 -v vector0,key,value"
helpVerbose = "-h --help | This help\n"
helpVerbose += "-L --localaddr | Address of the device: -L 10.50.0.50\n"
helpVerbose += "-M --netmask | Netmask of the device: -M 255.255.255.0\n"
helpVerbose += "-D --device | Device |name:type:path| if needed: -D vde,pic0,/tmp/pic0.ctl\n"
helpVerbose += "-g --gateway | Gateway to use: -g 10.50.0.1\n"
helpVerbose += "-d --init | Initialization of mdns: -i fubar.local,10.40.0.55"
helpVerbose += "-s --service | Service to add: -s WebServer,_http._tcp,80,vector0,30"
helpVerbose += "-k --kvector | Kvector to create: -k vector0"
helpVerbose += "-v --kvalue | Values of kvector: -v vector0,key,value"


def callback_register_sd(rtree,buff):
    global ended
    print "Service DNS-SD registred",buff,rtree
    ended = True

def callback_dnssd_sd(rtree,buff):
    global kvList
    global serviceList
    print "Arrived:",buff,rtree
    kvector = kvList[0]
    print "dnssdKVVectorAdd: "+str(pycotcp.dnssdKVVectorAdd(kvector[0],kvector[1],kvector[2]))
    for service in serviceList:
        print "dnssdRegisterService: "+str(pycotcp.dnssdRegisterService(service[0],service[1],service[2],service[3],service[4],callback_register_sd))
    

def main(argv):
    devtypeList = ["vde","tap","tun"]
    address = netmask = devName = devType = devPath = ""
    anyaddr = "0.0.0.0"
    gateway = ""
    hostnameInit = localaddr = ""
    try:
        opts, args = getopt.getopt(argv,"hL:M:D:g:i:k:s:v:",["help","localaddr=","netmask=","device=","gateway=","init=","kvector=","service=","kvalue="])
    except getopt.GetoptError:
        print "How to use: "+helpExample
        sys.exit(2)
    for opt, arg in opts:
        if opt in ("-h", "--help"):
            print helpVerbose
            print "Example: "+helpExample
            sys.exit()
        elif opt in ("-L", "--localaddr"):
            address = arg
        elif opt in ("-M", "--netmask"):
            netmask = arg
        elif opt in ("-D", "--device"):
            devArg = arg.split(separator)
            if (devArg[0] in devtypeList):
                devType = devArg[0]
                if len(devArg) == 1:
                    pass
                elif len(devArg) == 2:
                    devName = devArg[1]
                elif len(devArg) == 3:
                    devName = devArg[1]
                    devPath = devArg[2]
                else:
                    print "Error device"
                    sys.exit(2)
            else:
                print "Error device"
                sys.exit(2)
########################################################################        
        elif opt in ("-g", "--gateway"):
            gateway = arg
        elif opt in ("-i", "--init"):
            newarg = arg.split(separator)
            if len(newarg) != 2:
                print "Error init" + str(newarg)
                sys.exit(2)
            hostnameInit = newarg[0]
            localaddr = newarg[1]
            
        elif opt in ("-s", "--service"):
            newarg = arg.split(separator)
            if len(newarg) != 5:
                print "Error service" + str(newarg)
                sys.exit(2)
            serviceList.append(newarg)
            
        elif opt in ("-k", "--kvector"):
            newarg = arg.split(separator)
            if len(newarg) != 1:
                print "Error kvector" + str(newarg)
                sys.exit(2)
            vectorList.append(newarg)
            
        elif opt in ("-v", "--kvvalue"):
            newarg = arg.split(separator)
            if len(newarg) != 3:
                print "Error kvvalue" + str(newarg)
                sys.exit(2)
            kvList.append(newarg)
        else:
            print "Error arguments"
            sys.exit(2)
########################################################################
    if address == "" or netmask == "" or devName == "" or gateway == "" or hostnameInit == "":
        print "How to use: "+helpExample
        sys.exit(2)        
        

    pycotcp.init()
    if devName != "":
        if devPath != "":
            print "createDevice: "+str(pycotcp.createDevice(devType,devName,devPath))
        else:
           print "createDevice: "+str(pycotcp.createDevice(devType,devName)) 
    else:
        print "createDevice: "+str(pycotcp.createDevice(devType))
    print "linkAddIp4: "+ str(pycotcp.linkAddIp4(devName,address,netmask))
    print "routeAddIp4: "+ str(pycotcp.routeAddIp4(anyaddr,anyaddr,gateway,1,"null"))
    print "dnssdInit: "+ str(pycotcp.dnssdInit(hostnameInit,localaddr,callback_dnssd_sd))
    print "createKvVector: "+str(pycotcp.createKvVector(kvList[0][0]))
    try:
        while not ended:
            pycotcp.stackTick()
            pycotcp.idle()
        sys.exit()
    except KeyboardInterrupt:
        sys.exit()
    
if __name__ == "__main__":
   main(sys.argv[1:])
