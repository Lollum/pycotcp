import sys
import pycotcp
import time
import sys, getopt

ended = 0
separator = ","
searchList = []

helpExample = "dns.py -D vde,pic0,/tmp/pic0.ctl -L 10.40.0.55 --netmask 255.255.255.0 -g 10.40.0.1 -d 8.8.8.8,add -d 8.8.4.4,add -s www.google.it"
helpVerbose = "-h --help | This help\n"
helpVerbose += "-L --localaddr | Address of the device: -L 10.50.0.50\n"
helpVerbose += "-M --netmask | Netmask of the device: -M 255.255.255.0\n"
helpVerbose += "-D --device | Device |name:type:path| if needed: -D vde,pic0,/tmp/pic0.ctl\n"
helpVerbose += "-g --gateway | Gateway to use: -g 10.50.0.1\n"
helpVerbose += "-d --dns |[multiple] DNS service to add/del: -d 8.8.8.8,add"
helpVerbose += "-s --search |[multiple] Name to search address: -s www.google.it"


def callback_dns(string,arg):
    global ended
    print "Result: "+ string +" with code: "+str(arg)
    ended+=1

def main(argv):
    devtypeList = ["vde","tap","tun"]
    address = netmask = devName = devType = devPath = ""
    anyaddr = "0.0.0.0"
    gateway = ""
    dnsList = []
    try:
        opts, args = getopt.getopt(argv,"hL:M:D:g:d:s:",["help","localaddr=","netmask=","device=","gateway=","dns=","search="])
    except getopt.GetoptError:
        print "How to use: "+helpExample
        sys.exit(2)
    for opt, arg in opts:
        if opt in ("-h", "--help"):
            print helpVerbose
            print "Example: "+helpExample
            sys.exit()
        elif opt in ("-L", "--localaddr"):
            address = arg
        elif opt in ("-M", "--netmask"):
            netmask = arg
        elif opt in ("-D", "--device"):
            devArg = arg.split(separator)
            if (devArg[0] in devtypeList):
                devType = devArg[0]
                if len(devArg) == 1:
                    pass
                elif len(devArg) == 2:
                    devName = devArg[1]
                elif len(devArg) == 3:
                    devName = devArg[1]
                    devPath = devArg[2]
                else:
                    print "Error device"
                    sys.exit(2)
            else:
                print "Error device"
                sys.exit(2)
########################################################################        
        elif opt in ("-g", "--gateway"):
            gateway = arg
        elif opt in ("-d", "--dns"):
            newarg = arg.split(separator)
            if len(newarg) != 2:
                print "Error dns" + str(newarg)
                sys.exit(2)
            dnsList.append(newarg)
        elif opt in ("-s", "--search"):
            newarg = arg.split(separator)
            if len(newarg) != 1:
                print "Error search" + str(newarg)
                sys.exit(2)
            searchList.append(newarg)
        else:
            print "Error arguments"
            sys.exit(2)
########################################################################
    if address == "" or netmask == "" or devName == "" or gateway == "":
        print "How to use: "+helpExample
        sys.exit(2)        
        

    pycotcp.init()
    if devName != "":
        if devPath != "":
            print "createDevice: "+str(pycotcp.createDevice(devType,devName,devPath))
        else:
           print "createDevice: "+str(pycotcp.createDevice(devType,devName)) 
    else:
        print "createDevice: "+str(pycotcp.createDevice(devType))
    print "linkAddIp4: "+ str(pycotcp.linkAddIp4(devName,address,netmask))
    print "routeAddIp4: "+ str(pycotcp.routeAddIp4(anyaddr,anyaddr,gateway,1,"null"))
    for dns in dnsList:
        err = pycotcp.dnsNameServer(dns[0],dns[1])
        print "dnsNameServer: "+ str(err)
        if err < 0:
            print pycotcp.getPicoError()
    for search in searchList:
        err = pycotcp.dnsGetAddr(search[0],callback_dns)
        print "dnsGetName: "+ str(err)
        if err < 0:
            print pycotcp.getPicoError()
            
    try:
        while ended < len(searchList):
            pycotcp.stackTick()
            pycotcp.idle()
        sys.exit()
    except KeyboardInterrupt:
        sys.exit()
    
if __name__ == "__main__":
   main(sys.argv[1:])
