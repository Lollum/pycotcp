import sys
import pycotcp
import time
import getopt
import os
import io
ended = False
separator = ","
ipType = ""

helpExample = "tftpServer.py -D vde,pic0,/tmp/pic0.ctl -L 10.40.0.2 --netmask 255.255.255.0 -t ipv4 -f DemoFile/Result/CantoVentunoBis.txt"
helpVerbose = "######## WARNING: Due how Python handle files and String, this demo is designed with text file in mind #######\n"
helpVerbose += "-h --help | This help\n"
helpVerbose += "-L --localaddr | Address of the device: -L 10.50.0.50\n"
helpVerbose += "-M --netmask | Netmask of the device: -M 255.255.255.0\n"
helpVerbose += "-D --device | Device |name:type:path| if needed: -D vde,pic0,/tmp/pic0.ctl\n"
helpVerbose += "-t --type | Protocol to use: -t ipv4\n"
helpVerbose += "-f --filename | Filename used when receive: -f DemoFile/Result/CantoVentunoBis.txt\n"
helpVerbose += "-e --encoding | Encoding of the file, default utf-8: -e utf-8"

filetransfert = 0
filename = ""
encoding = "utf-8"

def callback_tftptx(session,event,block,leng,note):
    global ended
    if event == pycotcp.TFTP_EV_ERR_PEER:
        print "Remote Error"
        ended = True
    elif event == pycotcp.TFTP_EV_OK:
        send = pycotcp.tftpSend(session,note)
        print "Send block: "+str(send)
        if (send[0] < 512):
            print "[TX] Transfer Complete"
            ended = True
    else:
        print "Event receive: "+str(event)

def callback_tftprx(session,event,block,leng,note):
    global filetransfert
    global filename
    global ended
    global encoding
    if event == pycotcp.TFTP_EV_ERR_PEER:
        print "Remote Error"
        ended = True
    elif event == pycotcp.TFTP_EV_OK:
        if filetransfert == 0:
            f = io.open(filename, 'w')
        else:
            f = io.open(filename, 'a')
        filetransfert += leng
        print "Recv: "+str(filetransfert)
        try:
            towrite = block[:leng].decode(encoding) ##stringa sporca
            f.write(towrite)
        except Exception,e:
            print str(e)
        if leng < 512:
            print "[RX] Transfer Complete"        
            ended = True
    else:
        print "Event receive: "+str(event)
        
def callback_tftp_req(peer,port,opcode,filename,slen):
    global ipType
    print "callback_tftplisten: "+ peer +"|"+ str(port)+"|"+ str(opcode)+"|"+ str(filename)+"|"+ str(slen)
    if opcode == pycotcp.TFTP_RRQ:
        if not os.path.exists(filename):
            print "tftpRejectRequest: "+str(pycotcp.tftpRejectRequest(peer,port,"enoent","File not found"))
        else:
            result = (pycotcp.tftpSessionSetup(ipType,filename,peer))
            print "tftpSessionSetup: "+str(result)
            filelen = os.path.getsize(filename)
            print "tftpStartTx: "+str(pycotcp.tftpStartTx(port,filename,callback_tftptx,result[0],result[1]))
    elif opcode == pycotcp.TFTP_WRQ:
        result = (pycotcp.tftpSessionSetup(ipType,filename,peer))
        print "tftpSessionSetup: "+str(result)
        print "tftpStartRx: "+str(pycotcp.tftpStartRx(port,filename,callback_tftprx,result[0],result[1]))
    else:
        return "Code recv: "+str(opcode)

def main(argv):
    global ipType
    global filetransfert
    global filename
    global encoding
    devtypeList = ["vde","tap","tun"]
    address = netmask = devName = devType = devPath = ""
    dnsList = []
    try:
        opts, args = getopt.getopt(argv,"hL:M:D:t:f:e:",["help","localaddr=","netmask=","device=","type=","filename=","encoding="])
    except getopt.GetoptError:
        print "How to use: "+helpExample
        sys.exit(2)
    for opt, arg in opts:
        if opt in ("-h", "--help"):
            print helpVerbose
            print "Example: "+helpExample
            sys.exit()
        elif opt in ("-L", "--localaddr"):
            address = arg
        elif opt in ("-M", "--netmask"):
            netmask = arg
        elif opt in ("-D", "--device"):
            devArg = arg.split(separator)
            if (devArg[0] in devtypeList):
                devType = devArg[0]
                if len(devArg) == 1:
                    pass
                elif len(devArg) == 2:
                    devName = devArg[1]
                elif len(devArg) == 3:
                    devName = devArg[1]
                    devPath = devArg[2]
                else:
                    print "Error device"
                    sys.exit(2)
            else:
                print "Error device"
                sys.exit(2)
########################################################################        
        elif opt in ("-t", "--type"):
            ipType = arg
        elif opt in ("-e", "--encoding"):
            encoding = arg
        elif opt in ("-f", "--filename"):
            filename = arg
            filetransfert = 0
        else:
            print "Error arguments"
            sys.exit(2)
########################################################################
    if address == "" or netmask == "" or devName == "" or ipType == "":
        print "How to use: "+helpExample
        sys.exit(2)        
        
    pycotcp.init()
    if devName != "":
        if devPath != "":
            print "createDevice: "+str(pycotcp.createDevice(devType,devName,devPath))
        else:
           print "createDevice: "+str(pycotcp.createDevice(devType,devName)) 
    else:
        print "createDevice: "+str(pycotcp.createDevice(devType))
    print "linkAddIp4: "+ str(pycotcp.linkAddIp4(devName,address,netmask))
    print "tftpListen: "+ str(pycotcp.tftpListen(ipType,callback_tftp_req))    
            
    try:
        while not ended:
            pycotcp.stackTick()
            pycotcp.idle()
        sys.exit()
    except KeyboardInterrupt:
        sys.exit()
    
if __name__ == "__main__":
   main(sys.argv[1:])
