import pycotcp
import sys, getopt

helpExample = "socketEcho.py -D vde,pic0,/tmp/pic0.ctl -L 10.40.0.55 --netmask 255.255.255.0 -o socket0,ipv4,udp -b socket0,10.40.0.55,6667"
helpVerbose = "-h --help | This help\n"
helpVerbose += "-L --localaddr | Address of the device: -L 10.50.0.55\n"
helpVerbose += "-M --netmask | Netmask of the device: -M 255.255.255.0\n"
helpVerbose += "-L --device | Device |name,type,path| if needed: -D vde,pic0,/tmp/pic0.ctl\n"

helpVerbose += "-o --open | Open a socket |socket,ip_type,protocol|: -o socket0,ipv4,udp\n"
helpVerbose += "-b --bind | Bind a socket |socket,local_address,local_port|: -b socket0,10.40.0.55,6667\n"
helpVerbose += "-l --listen | Set the server to listen with max active clients |socket,max_client|: -l socket0,10\n"

commType = ""
separator = ","
closeConn = False
pckDim = 512

def callback_socket_udp(param,socket):
    global commType
    global closeConn
    global pckDim
    if (param == "RD"):
        recv = pycotcp.socketRecvFromExt(socket,pckDim,commType)
        if (recv[0] == "END"):
            closeConn = True
            print "socketSendTo Signal " + str(pycotcp.socketSendTo(socket,"END",4,str(recv[1]),str(recv[2])))
        else:
            print "socketRecvFromExt: " + str(recv)
    else:
        print "socketEcho: "+str(param)

def callback_socket_tcp(param,socket):
    global closeConn
    global pckDim
    if (param == "RD"):
        res = pycotcp.socketRead(socket,pckDim)
        print "socketRead:"+str(res)
    elif(param == "WR"):
        print "socketShutdown: "+ str(pycotcp.socketShutdown(socket,"w"))
    elif (param == "CONN"):
        acceptRes = pycotcp.socketAccept(socket)
        print "socketAccept: "+ str(acceptRes)
        print "socketSetOption: "+ str(pycotcp.socketSetOption(acceptRes[0],pycotcp.TCP_NODELAY,1))
        print "socketSetOption: "+ str(pycotcp.socketSetOption(acceptRes[0],pycotcp.SOCKET_OPT_KEEPCNT,5))
        print "socketSetOption: "+ str(pycotcp.socketSetOption(acceptRes[0],pycotcp.SOCKET_OPT_KEEPCNT,30000))
        print "socketSetOption: "+ str(pycotcp.socketSetOption(acceptRes[0],pycotcp.SOCKET_OPT_KEEPCNT,5000))
    elif(param == "CLOSE"):
        print "Client closed connection"
        closeConn = True
    elif(param == "FIN"):
        print "Socket closed"
        closeConn = True

def main(argv):
    devtypeList = ["vde","tap","tun"]
    address = netmask = devName = devType = devPath = ""
    protocolO = versionO = sockNameO = localportB = sockNameB = addrB = ""
    socketC = addrRC = portRC = ""
    socketL = ""
    queueL = 10
    sendList = []
    sendToList = []
    sendToExtList = []
    global commType
    
    try:
        opts, args = getopt.getopt(argv,"hL:M:D:o:b:s:S:e:c:l:",["help","localaddr=","netmask=","device=","open=","bind=","connect=","listen="])
    except getopt.GetoptError:
        print "How to use: "+helpExample
        sys.exit(2)
    for opt, arg in opts:
        if opt in ("-h", "--help"):
            print helpVerbose
            print "Example: "+helpExample
            sys.exit()
        elif opt in ("-L", "--localaddr"):
            address = arg
        elif opt in ("-M", "--netmask"):
            netmask = arg
        elif opt in ("-D", "--device"):
            devArg = arg.split(separator)
            if (devArg[0] in devtypeList):
                devType = devArg[0]
                if len(devArg) == 1:
                    pass
                elif len(devArg) == 2:
                    devName = devArg[1]
                elif len(devArg) == 3:
                    devName = devArg[1]
                    devPath = devArg[2]
                else:
                    print "Error device"
                    sys.exit(2)
            else:
                print "Error device"
                sys.exit(2)
#######################################################################        
        elif opt in ("-o", "--open"):
            newArg = arg.split(separator)
            print newArg
            if len(newArg) != 3:
                print "Error open" + str(newArg)
                sys.exit(2)
            socketL = sockNameO = newArg[0]
            commType = versionO = newArg[1]
            protocolO = newArg[2]
        
        elif opt in ("-b", "--bind"):
            newArg = arg.split(separator)
            if len(newArg) != 3:
                print "Error bind" + str(newArg)
                sys.exit(2)
            sockNameB = newArg[0]
            addrB = newArg[1]
            localportB = newArg[2]
            
        elif opt in ("-c", "--connect"):
            newArg = arg.split(separator)
            if len(newArg) != 3:
                print "Error connect" + str(newArg)
                sys.exit(2)
            socketC = newArg[0]
            addrRC = newArg[1]
            portRC = newArg[2]

        elif opt in ("-l", "--listen"):
            newArg = arg.split(separator)
            print newArg
            if len(newArg) != 2:
                print "Error listen" + str(newArg)
                sys.exit(2)
            socketL = newArg[0]
            queueL = int(newArg[1])
                   
        else:
            print "Error arguments"
            sys.exit(2)
    if address == "" or netmask == "" or devName == "":
        print "How to use: "+helpExample
        sys.exit(2)
#######################################################################  
    
    pycotcp.init()
    if devName != "":
        if devPath != "":
            print "createDevice: "+str(pycotcp.createDevice(devType,devName,devPath))
        else:
           print "createDevice: "+str(pycotcp.createDevice(devType,devName)) 
    else:
        print "createDevice: "+str(pycotcp.createDevice(devType))
    print "linkAddIp4: "+ str(pycotcp.linkAddIp4(devName,address,netmask))
    if (protocolO  == ""  or  versionO  == ""  or sockNameO  == "" ):
        print "no open arguments"
        sys.exit(2)        
    if (protocolO == "udp"):
        print "socketOpenUDP: "+ str(pycotcp.socketOpen(sockNameO,versionO,protocolO,callback_socket_udp))
    elif (protocolO == "tcp"):
        print "socketOpenTCP: "+ str(pycotcp.socketOpen(sockNameO,versionO,protocolO,callback_socket_tcp))
    else:
        print "Wrong protocol"
    if (localportB == "" or  sockNameB == "" or addrB  == "" ):
        print "no bind arguments"
        sys.exit(2)
    print "socketBind: "+ str(pycotcp.socketBind(sockNameB,addrB,localportB));
    if (socketC != "" or  addrRC != "" or portRC != ""):
        print "socketConnect: "+ str(pycotcp.socketConnect(socketC,addrRC,portRC));
    if (protocolO == "tcp"):
        print "socketListen: "+ str(pycotcp.socketListen(socketL,queueL));
    
    try:
        while not closeConn:
            pycotcp.stackTick()
            pycotcp.idle()
        sys.exit()
    except KeyboardInterrupt:
        sys.exit()
    
if __name__ == "__main__":
   main(sys.argv[1:])
