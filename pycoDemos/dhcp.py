import sys
import pycotcp

import sys, getopt

getAddrs = False
separator = ","

helpExample = "\nServer: dhcp.py -D vde,pic0,/tmp/pic0.ctl -L 10.40.0.50 --netmask 255.255.255.0 -s pic0,10.40.0.50,255.255.255.0,20,30,120\n"
helpExample += "Client: dhcp.py -D vde,pic0,/tmp/pic0.ctl -L 10.40.0.55 --netmask 255.255.255.0 -c pic0"
helpVerbose = "-h --help | This help\n"
helpVerbose += "-L --localaddr | Address of the device: -L 10.50.0.50\n"
helpVerbose += "-M --netmask | Netmask of the device: -M 255.255.255.0\n"
helpVerbose += "-D --device | Device |name:type:path| if needed: -D vde,pic0,/tmp/pic0.ctl\n"
helpVerbose += "-c --client | Initiate as dhcp client: -c pic0"
helpVerbose += "-s --server | Initiate as dhcp server |dev_name,local_addr,netmask,pool_start,pool_end,lease_time| : -s pic0,10.40.0.10,255.255.255.0,20,30,120"

def callback_dhcp(code,address,netmask,nameserver,xid):
    global getAddrs
    print "DHCP ASSIGNED AN ADDRESS!"
    print "dhcp: "+ str(code)+" : "+ str(address)+" : "+ str(netmask)+" : "+ str(nameserver)+" : "+ str(xid)
    getAddrs = True

def main(argv):
    devtypeList = ["vde","tap","tun"]
    address = netmask = devName = devType = devPath = ""
    link = ""
    dhcpDevC = ""
    dhcpDevS = localAddrS = netmaskS = startPoolS = endPoolS = leaseTimeS = ""
    try:
        opts, args = getopt.getopt(argv,"hL:M:D:s:c:",["help","localaddr=","netmask=","device=","server=","client="])
    except getopt.GetoptError:
        print "How to use: "+helpExample
        sys.exit(2)
    for opt, arg in opts:
        if opt in ("-h", "--help"):
            print helpVerbose
            print "Example: "+helpExample
            sys.exit()
        elif opt in ("-L", "--localaddr"):
            address = arg
        elif opt in ("-M", "--netmask"):
            netmask = arg
        elif opt in ("-D", "--device"):
            devArg = arg.split(separator)
            if (devArg[0] in devtypeList):
                devType = devArg[0]
                if len(devArg) == 1:
                    pass
                elif len(devArg) == 2:
                    devName = devArg[1]
                elif len(devArg) == 3:
                    devName = devArg[1]
                    devPath = devArg[2]
                else:
                    print "Error device"
                    sys.exit(2)
            else:
                print "Error device"
                sys.exit(2)
########################################################################        
        elif opt in ("-c", "--client"):
            dhcpDevC = arg
        elif opt in ("-s", "--server"):
            newarg = arg.split(separator)
            if len(newarg) != 6:
                print "Error server" + str(newarg)
                sys.exit(2)
            dhcpDevS = newarg[0]
            localAddrS = newarg[1]
            netmaskS = newarg[2]
            startPoolS = newarg[3]
            endPoolS = newarg[4]
            leaseTimeS = newarg[5]
        else:
            print "Error arguments"
            sys.exit(2)
########################################################################
    if address == "" or netmask == "" or devName == "":
        print "How to use: "+helpExample
        sys.exit(2)        
        

    pycotcp.init()
    if devName != "":
        if devPath != "":
            print "createDevice: "+str(pycotcp.createDevice(devType,devName,devPath))
        else:
           print "createDevice: "+str(pycotcp.createDevice(devType,devName)) 
    else:
        print "createDevice: "+str(pycotcp.createDevice(devType))
    print "linkAddIp4: "+ str(pycotcp.linkAddIp4(devName,address,netmask))
    if (dhcpDevS != ""):
        print "dhcpServerInitiate: "+ str(pycotcp.dhcpServerInitiate(dhcpDevS,localAddrS,netmaskS,startPoolS,endPoolS,leaseTimeS))
    elif (dhcpDevC != ""):
        print "dhcpClientInitiate: "+ str(pycotcp.dhcpClientInitiate(dhcpDevC,callback_dhcp))
    else:
        print "No dhcp called"
        sys.exit(2)
    try:
        while not getAddrs:
            pycotcp.stackTick()
            pycotcp.idle()
        sys.exit()
    except KeyboardInterrupt:
        sys.exit()
    
if __name__ == "__main__":
   main(sys.argv[1:])
