import sys
import pycotcp

import sys, getopt

ended = False
separator = ","

helpExample = "slaacv4.py -D vde,pic0,/tmp/pic0.ctl -c pic0"
helpVerbose = "-h --help | This help\n"
helpVerbose += "-D --device | Device |name:type:path| if needed: -D vde,pic0,/tmp/pic0.ctl\n"
helpVerbose += "-c --claim | Start claim process on device: -c pic0\n"
def callback_slaacv4(code,ip):
    global ended
    if (code == "success"):
        print "Address claimed: "+str(ip)
        ended = True
    elif (code == "error"):
        print "Error"
        ended = True
    else:
        print "Unknown"
        ended = True
def main(argv):
    devtypeList = ["vde","tap","tun"]
    address = netmask = devName = devType = devPath = ""
    deviceClaim = ""
    try:
        opts, args = getopt.getopt(argv,"hD:c:",["help","device=","claim="])
    except getopt.GetoptError:
        print "How to use: "+helpExample
        sys.exit(2)
    for opt, arg in opts:
        if opt in ("-h", "--help"):
            print helpVerbose
            print "Example: "+helpExample
            sys.exit()
        elif opt in ("-D", "--device"):
            devArg = arg.split(separator)
            if (devArg[0] in devtypeList):
                devType = devArg[0]
                if len(devArg) == 1:
                    pass
                elif len(devArg) == 2:
                    devName = devArg[1]
                elif len(devArg) == 3:
                    devName = devArg[1]
                    devPath = devArg[2]
                else:
                    print "Error device"
                    sys.exit(2)
            else:
                print "Error device"
                sys.exit(2)
########################################################################        
        elif opt in ("-c", "--claim"):
            deviceClaim = arg
        else:
            print "Error arguments"
            sys.exit(2)
########################################################################
    print address,netmask,devName,deviceClaim
    if devName == "" or deviceClaim == "":
        print "How to use: "+helpExample
        sys.exit(2)        
        

    pycotcp.init()
    if devName != "":
        if devPath != "":
            print "createDevice: "+str(pycotcp.createDevice(devType,devName,devPath))
        else:
           print "createDevice: "+str(pycotcp.createDevice(devType,devName)) 
    else:
        print "createDevice: "+str(pycotcp.createDevice(devType))
    print "slaacv4ClaimIP: " +str(pycotcp.slaacv4ClaimIP(deviceClaim,callback_slaacv4))
    try:
        while not ended:
            pycotcp.stackTick()
            pycotcp.idle()
        sys.exit()
    except KeyboardInterrupt:
        sys.exit()
    
if __name__ == "__main__":
   main(sys.argv[1:])
