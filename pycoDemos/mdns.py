import sys
import pycotcp
import time
import sys, getopt

ended = False
separator = ","
changeList = []

helpExample = "mdns.py -D vde,pic0,/tmp/pic0.ctl -L 10.40.0.55 --netmask 255.255.255.0 -g 10.40.0.1 -i fubar.local,10.40.0.55 -c fubar.local,foobur.local"
helpVerbose = "-h --help | This help\n"
helpVerbose += "-L --localaddr | Address of the device: -L 10.50.0.50\n"
helpVerbose += "-M --netmask | Netmask of the device: -M 255.255.255.0\n"
helpVerbose += "-D --device | Device |name:type:path| if needed: -D vde,pic0,/tmp/pic0.ctl\n"
helpVerbose += "-g --gateway | Gateway to use: -g 10.50.0.1\n"
helpVerbose += "-d --init | Initialization of mdns: -i fubar.local,10.40.0.55"
helpVerbose += "-s --change |[multiple] Change old hostname with a new one: -c fubar.local,foobur.local"


def callback_mdns_record(buff,rtree):
    print "get record result:",uno,due
    
def callback_mdns_sd(buff,rtree):
    global changeList
    global ended
    print "Inizializzato con nome:",buff
    ended = True
    for change in changeList:
        if change[0] == buff:
            print "Have to change hostname!"
            print "mdnsSetHostname: "+ str(pycotcp.mdnsSetHostname(change[1]))
            ended = False
    print "mdnsGetHostname: "+ str(pycotcp.mdnsGetHostname())

def main(argv):
    devtypeList = ["vde","tap","tun"]
    address = netmask = devName = devType = devPath = ""
    anyaddr = "0.0.0.0"
    gateway = ""
    hostnameInit = localaddr = ""
    try:
        opts, args = getopt.getopt(argv,"hL:M:D:g:i:c:",["help","localaddr=","netmask=","device=","gateway=","init=","change="])
    except getopt.GetoptError:
        print "How to use: "+helpExample
        sys.exit(2)
    for opt, arg in opts:
        if opt in ("-h", "--help"):
            print helpVerbose
            print "Example: "+helpExample
            sys.exit()
        elif opt in ("-L", "--localaddr"):
            address = arg
        elif opt in ("-M", "--netmask"):
            netmask = arg
        elif opt in ("-D", "--device"):
            devArg = arg.split(separator)
            if (devArg[0] in devtypeList):
                devType = devArg[0]
                if len(devArg) == 1:
                    pass
                elif len(devArg) == 2:
                    devName = devArg[1]
                elif len(devArg) == 3:
                    devName = devArg[1]
                    devPath = devArg[2]
                else:
                    print "Error device"
                    sys.exit(2)
            else:
                print "Error device"
                sys.exit(2)
########################################################################        
        elif opt in ("-g", "--gateway"):
            gateway = arg
        elif opt in ("-i", "--init"):
            newarg = arg.split(separator)
            if len(newarg) != 2:
                print "Error init" + str(newarg)
                sys.exit(2)
            hostnameInit = newarg[0]
            localaddr = newarg[1]
        elif opt in ("-c", "--change"):
            newarg = arg.split(separator)
            if len(newarg) != 2:
                print "Error change" + str(newarg)
                sys.exit(2)
            changeList.append(newarg)
        else:
            print "Error arguments"
            sys.exit(2)
########################################################################
    if address == "" or netmask == "" or devName == "" or gateway == "" or hostnameInit == "":
        print "How to use: "+helpExample
        sys.exit(2)        
        

    pycotcp.init()
    if devName != "":
        if devPath != "":
            print "createDevice: "+str(pycotcp.createDevice(devType,devName,devPath))
        else:
           print "createDevice: "+str(pycotcp.createDevice(devType,devName)) 
    else:
        print "createDevice: "+str(pycotcp.createDevice(devType))
    print "linkAddIp4: "+ str(pycotcp.linkAddIp4(devName,address,netmask))
    print "routeAddIp4: "+ str(pycotcp.routeAddIp4(anyaddr,anyaddr,gateway,1,"null"))
    print "mdnsInit: "+ str(pycotcp.mdnsInit(hostnameInit,localaddr,callback_mdns_sd))
    try:
        while not ended:
            pycotcp.stackTick()
            pycotcp.idle()
        sys.exit()
    except KeyboardInterrupt:
        sys.exit()
    
if __name__ == "__main__":
   main(sys.argv[1:])
