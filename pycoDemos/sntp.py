import sys
import pycotcp
import time
import sys, getopt

ended = False
separator = ","

helpExample = "sntp.py -D vde,pic0,/tmp/pic0.ctl -L 10.40.0.55 --netmask 255.255.255.0 -g 10.40.0.1 -n 0.it.pool.ntp.org"
helpVerbose = "-h --help | This help\n"
helpVerbose += "-L --localaddr | Address of the device: -L 10.50.0.50\n"
helpVerbose += "-M --netmask | Netmask of the device: -M 255.255.255.0\n"
helpVerbose += "-D --device | Device |name:type:path| if needed: -D vde,pic0,/tmp/pic0.ctl\n"
helpVerbose += "-g --gateway | Gateway to use: -g 10.50.0.1\n"
helpVerbose += "-n --ntp | NTP service to call: -n 0.it.pool.ntp.org"

def callback_sntp(code):
    global ended
    if (code == "notconn"):
        print "Error: not connected"
    elif (code == "inval"):
        print "Error: invalid"
    elif (code == "timedout"):
        print "Error: timeout"
    elif (code == "netdown"):
        print "Error: network down"
    elif (code == "ok"):
        timeres = pycotcp.sntpGetTimeOfTheDay()
        print "sntpGetTimeOfTheDay: " +str(timeres)
        print "Date: "+str(time.ctime(timeres[0]))
    else:
        print "Error: unknown "+str(code)
    ended = True

def main(argv):
    devtypeList = ["vde","tap","tun"]
    address = netmask = devName = devType = devPath = ""
    gateway = ""
    ntp = ""
    anyaddr = "0.0.0.0"
    try:
        opts, args = getopt.getopt(argv,"hL:M:D:g:n:",["help","localaddr=","netmask=","device=","gateway=","ntp="])
    except getopt.GetoptError:
        print "How to use: "+helpExample
        sys.exit(2)
    for opt, arg in opts:
        if opt in ("-h", "--help"):
            print helpVerbose
            print "Example: "+helpExample
            sys.exit()
        elif opt in ("-L", "--localaddr"):
            address = arg
        elif opt in ("-M", "--netmask"):
            netmask = arg
        elif opt in ("-D", "--device"):
            devArg = arg.split(separator)
            if (devArg[0] in devtypeList):
                devType = devArg[0]
                if len(devArg) == 1:
                    pass
                elif len(devArg) == 2:
                    devName = devArg[1]
                elif len(devArg) == 3:
                    devName = devArg[1]
                    devPath = devArg[2]
                else:
                    print "Error device"
                    sys.exit(2)
            else:
                print "Error device"
                sys.exit(2)
########################################################################        
        elif opt in ("-g", "--gateway"):
            gateway = arg
        elif opt in ("-n", "--ntp"):
            ntp = arg
        else:
            print "Error arguments"
            sys.exit(2)
########################################################################
    if address == "" or netmask == "" or devName == "" or gateway == "" or ntp == "":
        print "How to use: "+helpExample
        sys.exit(2)        
        

    pycotcp.init()
    if devName != "":
        if devPath != "":
            print "createDevice: "+str(pycotcp.createDevice(devType,devName,devPath))
        else:
           print "createDevice: "+str(pycotcp.createDevice(devType,devName)) 
    else:
        print "createDevice: "+str(pycotcp.createDevice(devType))
    print "linkAddIp4: "+ str(pycotcp.linkAddIp4(devName,address,netmask))
    print "routeAddIp4: "+ str(pycotcp.routeAddIp4(anyaddr,anyaddr,gateway,1,"null"))
    print "sntpSync: " +str(pycotcp.sntpSync(ntp,callback_sntp))
    try:
        while not ended:
            pycotcp.stackTick()
            pycotcp.idle()
        sys.exit()
    except KeyboardInterrupt:
        sys.exit()
    
if __name__ == "__main__":
   main(sys.argv[1:])
