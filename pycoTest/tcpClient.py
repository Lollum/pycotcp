from ctypes import *
import time
import pycotcp

indirizzo_locale = "aaaa::2"
indirizzo_remoto = "aaaa::1"
indirizzo_any = "::0"
mask = "ffff::"
porta_locale = "6667"
porta_remoto = "6667"

def callback_socket(param,socket):
    print "socketEcho: "+param
    if (param == "CONN"):
        print "Connesso al server"
    elif (param == "RD"):
        res = pycotcp.socketRead(socket,100)
        print "socketRead:"+str(res)
    elif(param == "WR"):
        print "socketWrite: "+ str(pycotcp.socketWrite(socket,"MANDO DA CLIENT",100))
        print "socketWrite: "+ str(pycotcp.socketShutdown(socket,"w"))
    elif(param == "CLOSE"):
        print "Server chiuso connessione"
    elif(param == "FIN"):
        print "Socket chiuso"
    else:
        print param

pycotcp.init()
print "createDevice:" + str(pycotcp.createDevice("vde","pic0","/tmp/pic0.ctl"))
print "linkAddIp4: "+ str(pycotcp.linkAddIp4("pic0",indirizzo_locale,mask))
print "socketOpen: "+ str(pycotcp.socketOpen("socket1","ipv4","tcp",callback_socket))
print "socketOpen: "+ str(pycotcp.socketSetOption("socket1",pycotcp.TCP_NODELAY,1))
print "socketBind: "+ str(pycotcp.socketBind("socket1",indirizzo_locale,porta_locale));
print "socketConnect: "+ str(pycotcp.socketConnect("socket1",indirizzo_remoto,porta_remoto));
print "socketGetName: "+ str(pycotcp.socketGetName("socket1","ipv4"));
print "socketGetPeerName: "+ str(pycotcp.socketGetPeerName("socket1","ipv4"));
try:
    while True:
        pycotcp.stackTick()
        pycotcp.idle()
except KeyboardInterrupt:
    print "socketClose: "+ str(pycotcp.socketClose("socket1"))
    pycotcp.stackTick()
    exit
