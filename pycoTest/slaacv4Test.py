from ctypes import *
import pycotcp
import sys

remote_addr = "169.254.22.5"
toExit = False

def callback_ping(dest,size,seq,time,ttl,err):
    if (err == 0):
        print "ricevuto: "+str(dest)+" size:"+str(size)+" seq:"+str(seq)+" time:"+str(time)+" ttl:"+str(ttl)+" err:"+str(err)
        if (seq == 5):
            print "slaacv4UnregisterIP: "+str(pycotcp.slaacv4UnregisterIP())
    else:
        print "++ERRORE: "+str(dest)+" seq:"+str(seq)+" err:"+str(err) 

def callback_slaacv4(code,ip):
    print "ricevuto: "+str(code)+" : "+str(ip)
    if (code == "success"):
        pycotcp.pingStartIp4(remote_addr,5,1000,3000,32,callback_ping)

pycotcp.init()
print "inizializzazione fatta "
print "createDevice:" + str(pycotcp.createDevice("vde","pic0","/tmp/pic0.ctl"))
print "slaacv4ClaimIP: " +str(pycotcp.slaacv4ClaimIP("pic0",callback_slaacv4))
try:
    while not toExit:
        pycotcp.stackTick()
        pycotcp.idle()
except KeyboardInterrupt:
    exit
