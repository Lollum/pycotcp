from ctypes import *
import pycotcp
import time
import os
localaddr = "10.40.0.2"
localmask = "255.255.255.0"
anyaddr = "0.0.0.0"
gateway = "10.40.0.1"
ntp = "0.it.pool.ntp.org"
hostname = "host.local";

def callback_tftprxtx(session,event,block,leng,note):
    if event == pycotcp.TFTP_EV_OK:
        send = pycotcp.tftpSend(session,note)
        print "Mando blocco: "+str(send)
        if (send[0] < 512):
            print "Trasferimento Completato"
        
        
def callback_tftplisten(peer,port,opcode,filename,slen):
    print "callback_tftplisten: "+ peer +"|"+ str(port)+"|"+ str(opcode)+"|"+ str(filename)+"|"+ str(slen)
    if opcode == pycotcp.TFTP_RRQ:
        if not os.path.exists(filename):
            print "tftpRejectRequest: "+str(pycotcp.tftpRejectRequest(peer,port,"enoent","File not found"))
        else:
            result = (pycotcp.tftpSessionSetup("ipv4",filename,peer))
            print "tftpSessionSetup: "+str(result)
            filelen = os.path.getsize(filename)
            #print "tftpSetOption: "+str(pycotcp.tftpSetOption(result[0],'file',filelen))
            print "tftpStartTx: "+str(pycotcp.tftpStartTx(port,filename,callback_tftprxtx,result[0],result[1]))

pycotcp.init()
print "inizializzazione fatta "
print "createDevice:" + str(pycotcp.createDevice("vde","pic0","/tmp/pic0.ctl"))
print "linkAddIp4: "+ str(pycotcp.linkAddIp4("pic0",localaddr,localmask))
print "tftpListen: "+ str(pycotcp.tftpListen("ipv4",callback_tftplisten))


try:
    while True:
        pycotcp.stackTick()
        pycotcp.idle()
except KeyboardInterrupt:
    exit
