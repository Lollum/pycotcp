from ctypes import *
import pycotcp
import io
localaddr = "10.40.0.2"
localmask = "255.255.255.0"
anyaddr = "0.0.0.0"
gateway = "10.40.0.1"
ntp = "0.it.pool.ntp.org"
hostname = "host.local";
filetransfert = 0
filepath = "../testTftp/result/"
filename = filepath+"CantoVentuno.txt"

def callback_tftprxtx(session,event,block,leng,note):
    global filetransfert
    global filename
    if event == pycotcp.TFTP_EV_OPT:
        print "Dovrei ricevere: "+str(pycotcp.tftpGetOption(session,'file'))
    if event == pycotcp.TFTP_EV_OK:
        if filetransfert == 0:
            f = io.open(filename, 'w')
        else:
            f = io.open(filename, 'a')
        filetransfert += leng
        print "ricevo: "+str(filetransfert)
        try:
            towrite = block[:leng].decode('utf-8') ##stringa sporca
            f.write(towrite)
        except Exception,e:
            print str(e)
        if leng < 512:
            print "completato"
            
def callback_tftplisten(peer,port,opcode,filename,slen):
    print "callback_tftplisten: "+ peer +"|"+ str(port)+"|"+ str(opcode)+"|"+ str(filename)+"|"+ str(slen)
    if opcode == pycotcp.TFTP_WRQ:
        result = (pycotcp.tftpSessionSetup("ipv4",filename,peer))
        print "tftpSessionSetup: "+str(result)
        print "tftpParseRequestArgs: "+str(pycotcp.tftpParseRequestArgs(filename,slen,result[1]))
        print "tftpSetOption server: "+str(pycotcp.tftpSetOption(result[0],'file',result[1]))
        print "tftpStartRx: "+str(pycotcp.tftpStartRx(port,filename,callback_tftprxtx,result[0],result[1]))
            
pycotcp.init()
print "inizializzazione fatta "
print "createDevice:" + str(pycotcp.createDevice("vde","pic0","/tmp/pic0.ctl"))
print "linkAddIp4: "+ str(pycotcp.linkAddIp4("pic0",localaddr,localmask))
print "tftpListen: "+ str(pycotcp.tftpListen("ipv4",callback_tftplisten))

try:
    while True:
        pycotcp.stackTick()
        pycotcp.idle()
except KeyboardInterrupt:
    exit
