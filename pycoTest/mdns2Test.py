from ctypes import *
import pycotcp
import time

localaddr = "10.40.0.5"
localmask = "255.255.255.0"
anyaddr = "0.0.0.0"
gateway = "10.40.0.1"
ntp = "0.it.pool.ntp.org"
hostname = "host.local";
def callback_mdns_record(buff,rtree):
    print "get record result:",buff,rtree
    
def callback_mdns_sd(buff,rtree):
    print "Inizializzato con nome:",buff
    if buff == "host.local":
        print "mdnsSetHostname: "+ str(pycotcp.mdnsSetHostname("hostfoo.local"))
    print "mdnsGetHostname: "+ str(pycotcp.mdnsGetHostname())
    

pycotcp.init()
print "inizializzazione fatta "
print "createDevice:" + str(pycotcp.createDevice("vde","pic0","/tmp/pic0.ctl"))
print "linkAddIp4: "+ str(pycotcp.linkAddIp4("pic0",localaddr,localmask))
print "routeAddIp4: "+ str(pycotcp.routeAddIp4(anyaddr,anyaddr,gateway,1,"null"))
print "mdnsInit: "+ str(pycotcp.mdnsInit(hostname,localaddr,callback_mdns_sd))
print "mdnsGetRecord: "+ str(pycotcp.mdnsGetRecord("record0","ptr",callback_mdns_record))

try:
    while True:
        pycotcp.stackTick()
        pycotcp.idle()
except KeyboardInterrupt:
    exit
