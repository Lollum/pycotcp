from ctypes import *
import pycotcp
import io
import os
localaddr = "10.40.0.3"
remoteaddr = "10.40.0.2"
localmask = "255.255.255.0"
anyaddr = "0.0.0.0"
gateway = "10.40.0.1"
ntp = "0.it.pool.ntp.org"
hostname = "host.local";
filetransfert = 0
filepath = "../testTftp/"
filenameclient = filepath+"canto21.txt"

def callback_tftprxtx(session,event,block,leng,note):
    if event == pycotcp.TFTP_EV_OK or event == pycotcp.TFTP_EV_OPT:
        send = pycotcp.tftpSend(session,note)
        print "Mando blocco: "+str(send[1])
        if (send[0] < 512):
            print "Trasferimento Completato"
                        
pycotcp.init()
print "inizializzazione fatta "
print "createDevice:" + str(pycotcp.createDevice("vde","pic0","/tmp/pic0.ctl"))
print "linkAddIp4: "+ str(pycotcp.linkAddIp4("pic0",localaddr,localmask))
result = (pycotcp.tftpSessionSetup("ipv4",filenameclient,remoteaddr))
print "tftpSessionSetup: "+str(result)
filelen = os.path.getsize(filenameclient)
print "tftpSetOption client: "+str(pycotcp.tftpSetOption(result[0],'file',filelen))
print "Dovrei trasmette: "+str(pycotcp.tftpGetOption(result[0],'file'))
print "tftpStartTx: "+str(pycotcp.tftpStartTx(pycotcp.TFTP_PORT,filenameclient,callback_tftprxtx,result[0],result[1]))
try:
    while True:
        pycotcp.stackTick()
        pycotcp.idle()
except KeyboardInterrupt:
    exit
