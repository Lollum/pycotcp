from ctypes import *
import pycotcp

def callback_ping(dest,size,seq,time,ttl,err):
    if (err == 0):
        print "ricevuto: "+str(dest)+" size:"+str(size)+" seq:"+str(seq)+" time:"+str(time)+" ttl:"+str(ttl)+" err:"+str(err)
    else:
        print "++ERRORE: "+str(dest)+" seq:"+str(seq)+" err:"+str(err)+"++" 

pycotcp.init()
print "inizializzazione fatta "
print "Device:" + str(pycotcp.createDevice("vde","pic0","/tmp/pic0.ctl"))
print "Device:" + str(pycotcp.createDevice("vde","pic1","/tmp/pic0.ctl"))
print "Device:" + str(pycotcp.createDevice("vde","pic2","/tmp/pic0.ctl"))
print "link: "+ str(pycotcp.linkAddIp4("pic0","10.40.0.55","255.255.255.0"))
print "link: "+ str(pycotcp.linkAddIp4("pic1","10.40.0.56","255.255.255.0"))
print "link: "+ str(pycotcp.linkAddIp4("pic2","10.40.0.57","255.255.255.0"))
id = pycotcp.pingStartIp4("10.40.0.3",10,1000,3000,64,callback_ping)
id = pycotcp.pingStartIp4("10.40.0.21",10,1000,3000,64,callback_ping)
id = pycotcp.pingStartIp4("10.40.0.15",10,1000,3000,64,callback_ping)
try:
    while True:
        pycotcp.stackTick()
        pycotcp.idle()
except KeyboardInterrupt:
    exit

#pycotcp.pingAbortIp4(id)
