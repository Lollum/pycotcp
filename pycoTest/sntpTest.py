from ctypes import *
import pycotcp
import time

localaddr = "10.40.0.5"
localmask = "255.255.255.0"
anyaddr = "0.0.0.0"
gateway = "10.40.0.1"
ntp = "0.it.pool.ntp.org"
def callback_sntp(code):
    print "ricevuto: "+str(code)
    if (code == "ok"):
        timeres = pycotcp.sntpGetTimeOfTheDay()
        print "sntpGetTimeOfTheDay: " +str(timeres)
        print "Date: "+str(time.ctime(timeres[0]))

pycotcp.init()
print "inizializzazione fatta "
print "createDevice:" + str(pycotcp.createDevice("vde","pic0","/tmp/pic0.ctl"))
print "linkAddIp4: "+ str(pycotcp.linkAddIp4("pic0",localaddr,localmask))
print "routeAddIp4: "+ str(pycotcp.routeAddIp4(anyaddr,anyaddr,gateway,1,"null"))
print "sntpGetTimeOfTheDayFail: " +str(pycotcp.sntpGetTimeOfTheDay())
print "sntpSync: " +str(pycotcp.sntpSync(ntp,callback_sntp))
try:
    while True:
        pycotcp.stackTick()
        pycotcp.idle()
except KeyboardInterrupt:
    exit
