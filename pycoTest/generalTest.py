from ctypes import *
import pycotcp

def callback_ping(dest,size,seq,time,ttl,err):
	print "ricevuto: "+str(dest)+" size:"+str(size)+" seq:"+str(seq)+" time:"+str(time)+" ttl:"+str(ttl)+" err:"+str(err)

pycotcp.init()

print "inizializzazione fatta "

print "Device pic0: " + str(pycotcp.createDevice("vde","pic0","/tmp/pic0.ctl"))
print "Device pic0: " + str(pycotcp.createDevice("vde","pic3","/tmp/pic0.ctl"))
print "Device pic0: " + str(pycotcp.createDevice("vde","pic1","/tmp/pic0.ctl"))
print "Device pic0: " + str(pycotcp.createDevice("vde","pic2","/tmp/pic0.ctl"))
print "Device pic0: " + str(pycotcp.createDevice("ppp"))
print "*****************Device list: " + str(pycotcp.testDevice())
print "link 5: "+ str(pycotcp.linkAddIp4("pic0","192.168.5.5","255.255.255.0"))
print "link 6: "+ str(pycotcp.linkAddIp4("pic1","192.168.5.6","255.255.255.0"))
print "link 7: "+ str(pycotcp.linkAddIp4("pic2","192.168.5.7","255.255.255.0"))
print "Device pic0: " + str(pycotcp.deleteDevice("pic3"))
print "*****************Device list: " + str(pycotcp.testDevice())
print "findlink 5: "+ str(pycotcp.linkFindIp4("192.168.5.5"))
print "findlink 6: "+ str(pycotcp.linkFindIp4("192.168.5.6"))
print "findlink 7: "+ str(pycotcp.linkFindIp4("192.168.5.7"))
print "findlink fail: "+ str(pycotcp.linkFindIp4("192.168.5.55"))
print "dellink 5: "+ str(pycotcp.linkDelIp4("pic0","192.168.5.5"))
print "findlink 5: "+ str(pycotcp.linkFindIp4("192.168.5.5"))
print "findlink 6: "+ str(pycotcp.linkFindIp4("192.168.5.6"))

print "routeAdd 7: "+ str(pycotcp.routeAddIp4("192.168.5.7","255.255.255.0","192.168.5.6",1,"NULL"))
print "routeGetGateway: "+ str(pycotcp.routeGetGatewayIp4("192.168.5.7")) ##DISTURBA LE ALTRE

print "routeDel 7: "+ str(pycotcp.routeDelIp4("192.168.5.7","255.255.255.0",1))
print "routeDel 7: "+ str(pycotcp.routeDelIp4("192.168.5.7","255.255.255.0",1))

print "portForwardAdd 7: "+ str(pycotcp.portForwardIp4("192.168.22.7","7412","192.168.5.7","2147","icmp4","add"))
print "portForwardDell 7: "+ str(pycotcp.portForwardIp4("192.168.22.7","7412","192.168.5.7","2147","icmp4","del"))
print "portForwardDellFail 7: "+ str(pycotcp.portForwardIp4("192.168.22.7","7412","192.168.5.7","2147","icmp4","del"))
try:
    while True:
        pycotcp.stackTick()
        pycotcp.idle()
except KeyboardInterrupt:
    exit
