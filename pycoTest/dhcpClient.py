from ctypes import *
import pycotcp
def callback_dhcp(code,address,netmask,nameserver,xid):
    print "dhcp: "+ str(code)+" : "+ str(address)+" : "+ str(netmask)+" : "+ str(nameserver)+" : "+ str(xid)

pycotcp.init()
print "inizializzazione fatta "
print "Device pic0: " + str(pycotcp.createDevice("vde","pic0","/tmp/pic0.ctl"))
print "linkAddIp4: "+ str(pycotcp.linkAddIp4("pic0","10.40.0.5","255.255.255.0"))
print "dhcpClientInitiate: "+ str(pycotcp.dhcpClientInitiate("pic0",callback_dhcp))
try:
    while True:
        pycotcp.stackTick()
        pycotcp.idle()
except KeyboardInterrupt:
    exit
