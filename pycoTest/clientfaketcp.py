from ctypes import *
import time
import pycotcp

indirizzo_locale = "10.40.0.60"
indirizzo_remoto = "10.40.0.50"
mask = "255.255.255.0"
porta_locale = "5555"
porta_remoto = "6667"


def callback_socket(param):
    if (param == "RD"):
        print "socketRecvFrom: "
    else:
        print "socketEcho: "+param

pycotcp.init()
print "createDevice:" + str(pycotcp.createDevice("vde","pic0","/tmp/pic0.ctl"))
print "linkAddIp4: "+ str(pycotcp.linkAddIp4("pic0",indirizzo_locale,mask))
print "socketOpen: "+ str(pycotcp.socketOpen("socket1","ipv4","tcp",callback_socket))
print "socketOpen: "+ str(pycotcp.socketSetOption("socket1",pycotcp.TCP_NODELAY,1))
print "socketBind: "+ str(pycotcp.socketBind("socket1",indirizzo_locale,porta_locale));
print "socketConnect: "+ str(pycotcp.socketConnect("socket1",indirizzo_remoto,porta_remoto));
print "socketGetName: "+ str(pycotcp.socketGetName("socket1"));
print "socketGetPeerName: "+ str(pycotcp.socketGetPeerName("socket1"));
#print "socketWrite: "+ str(pycotcp.socketWrite("socket1","This",100))
#print "socketClose: "+ str(pycotcp.socketClose("socket1"))
try:
    while True:
        pycotcp.stackTick()
        pycotcp.idle()
except KeyboardInterrupt:
    exit
