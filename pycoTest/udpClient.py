from ctypes import *
import time
import pycotcp

indirizzo_locale = "10.40.0.53"
indirizzo_remoto = "10.40.0.50"
mask = "255.255.255.0"
porta_locale = "6667"
porta_remoto = "5555"

def callback_socket(param,socket):
    if (param == "RD"):
        recv = pycotcp.socketRecvFrom(socket,100,"ipv4")
        print "socketRecvFrom: " + str(recv)
        if (recv[0] == "dududun"):
            print "socketSendToExt: "+ str(pycotcp.socketSendToExt(socket,"My only friend",100,indirizzo_remoto,porta_remoto,32,1))
    else:
        print "socketEcho: "+param

pycotcp.init()
print "inizializzazione VOCE fatta"
print "createDevice:" + str(pycotcp.createDevice("vde","pic0","/tmp/pic0.ctl"))
print "linkAddIp4: "+ str(pycotcp.linkAddIp4("pic0",indirizzo_locale,mask))
print "socketOpen: "+ str(pycotcp.socketOpen("socket1","ipv4","udp",callback_socket))
print "socketOpen: "+ str(pycotcp.socketOpen("socket2","ipv4","udp",callback_socket))
print "socketBind: "+ str(pycotcp.socketBind("socket2",indirizzo_locale,porta_locale));
print "socketConnect: "+ str(pycotcp.socketConnect("socket2",indirizzo_remoto,porta_remoto));
print "socketGetName: "+ str(pycotcp.socketGetName("socket2","ipv4"));
print "socketGetPeerName: "+ str(pycotcp.socketGetPeerName("socket2","ipv4"));
print "socketSend: "+ str(pycotcp.socketSend("socket2","This",100))
print "socketSend: "+ str(pycotcp.socketSend("socket2","is",100))
print "socketSendFail: "+ str(pycotcp.socketSend("socket1","NOISE",100))
print "getPicoError: "+ str(pycotcp.getPicoError())
print "socketSend: "+ str(pycotcp.socketSend("socket2","the",100))
print "socketSend: "+ str(pycotcp.socketSend("socket2","end",100))
try:
    while True:
        pycotcp.stackTick()
        pycotcp.idle()
except KeyboardInterrupt:
    exit
