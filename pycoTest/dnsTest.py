from ctypes import *
import pycotcp

localaddr = "10.40.0.5"
localmask = "255.255.255.0"
anyaddr = "0.0.0.0"
gateway = "10.40.0.1"
remoteaddr = "149.3.177.25" #google
remotename = "www.google.it" #google
dns1 = "8.8.8.8"
dns2 =  "8.8.4.4"
def callback_dns(string,arg):
    print "stringa :"+ string +" : "+str(arg)

pycotcp.init()
print "inizializzazione fatta "
print "createDevice:" + str(pycotcp.createDevice("vde","pic0","/tmp/pic0.ctl"))
print "linkAddIp4: "+ str(pycotcp.linkAddIp4("pic0",localaddr,localmask))
print "routeAddIp4: "+ str(pycotcp.routeAddIp4(anyaddr,anyaddr,gateway,1,"null"))
print "dnsNameServer: "+ str(pycotcp.dnsNameServer(dns1,"add"))
print "dnsNameServer: "+ str(pycotcp.dnsNameServer(dns2,"add"))
print "dnsGetName: "+ str(pycotcp.dnsGetName(remoteaddr,callback_dns))
print "dnsGetAddr: "+ str(pycotcp.dnsGetAddr(remotename,callback_dns))#non funziona neanche nel test di picotcp

try:
    while True:
        pycotcp.stackTick()
        pycotcp.idle()
except KeyboardInterrupt:
    exit
