from ctypes import *
import time
import pycotcp

indirizzo_locale = "aaaa::1"
indirizzo_remoto = "aaaa::2"
indirizzo_any = "::0"
mask = "ffff::"
porta_locale = "6667"
porta_remoto = "6667"

count = 0
def callback_socket(param,socket):
    print "socketEcho: "+param
    if (param == "RD"):
        res = pycotcp.socketRead(socket,100)
        print "socketRead:"+str(res)
    elif(param == "WR"):
        print "socketWrite: "+ str(pycotcp.socketWrite(socket,"MANDO DA ECHO",100))
        print "socketWrite: "+ str(pycotcp.socketShutdown(socket,"w"))
    elif (param == "CONN"):
        acceptRes = pycotcp.socketAccept("socket1")
        print "socketAccept: "+ str(acceptRes)
        print "socketSetOption: "+ str(pycotcp.socketSetOption(acceptRes[0],pycotcp.TCP_NODELAY,1))
        print "socketSetOption: "+ str(pycotcp.socketSetOption(acceptRes[0],pycotcp.SOCKET_OPT_KEEPCNT,5))
        print "socketSetOption: "+ str(pycotcp.socketSetOption(acceptRes[0],pycotcp.SOCKET_OPT_KEEPCNT,30000))
        print "socketSetOption: "+ str(pycotcp.socketSetOption(acceptRes[0],pycotcp.SOCKET_OPT_KEEPCNT,5000))
    elif(param == "CLOSE"):
        print "Client chiuso connessione"
    elif(param == "FIN"):
        print "Socket chiuso"
    else:
        print param
        
pycotcp.init()
print "createDevice:" + str(pycotcp.createDevice("vde","pic0","/tmp/pic0.ctl"))
print "linkAddIp4: "+ str(pycotcp.linkAddIp4("pic0",indirizzo_locale,mask))
print "socketOpen: "+ str(pycotcp.socketOpen("socket1","ipv4","tcp",callback_socket))
print "socketSetOption: "+ str(pycotcp.socketSetOption("socket1",pycotcp.TCP_NODELAY,1))
print "socketBind: "+ str(pycotcp.socketBind("socket1",indirizzo_locale,porta_locale));
print "socketGetName: "+ str(pycotcp.socketGetName("socket1","ipv4"));
print "socketGetPeerName: "+ str(pycotcp.socketGetPeerName("socket1","ipv4"));
print "socketListen: "+ str(pycotcp.socketListen("socket1",50));
try:
    while True:
        pycotcp.stackTick()
        pycotcp.idle()
except KeyboardInterrupt:
    print "socketClose: "+ str(pycotcp.socketClose("socket1"))
    pycotcp.stackTick()
    exit
