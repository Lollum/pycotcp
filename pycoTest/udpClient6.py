from ctypes import *
import time 
import pycotcp

indirizzo_locale = "aaaa::2"
indirizzo_remoto = "aaaa::1"
indirizzo_any = "::0"
mask = "ffff::"
porta_locale = "6667"
porta_remoto = "6667"

def callback_socket(param,socket):
    if (param == "RD"):
        recv = pycotcp.socketRecvFrom(socket,100,"ipv6")
        print "socketRecvFrom: " + str(recv)
        if (recv[0] == "dududun"):
            print "socketSendToExt: "+ str(pycotcp.socketSendToExt(socket,"My only friend",100,indirizzo_remoto,porta_remoto,32,1))
    else:
        print "socketEcho: "+param

pycotcp.init()
print "inizializzazione VOCE fatta"
print "createDevice:" + str(pycotcp.createDevice("vde","pic0","/tmp/pic0.ctl"))
print "linkAddIp6: "+ str(pycotcp.linkAddIp6("pic0",indirizzo_locale,mask))
print "routingEnableIpv6: "+ str(pycotcp.routingEnableIpv6("pic0"));
print "socketOpen: "+ str(pycotcp.socketOpen("socket1","ipv6","udp",callback_socket))
print "socketBind: "+ str(pycotcp.socketBind("socket1",indirizzo_any,porta_locale));
print "socketConnect: "+ str(pycotcp.socketConnect("socket1",indirizzo_remoto,porta_remoto));
print "socketGetName: "+ str(pycotcp.socketGetName("socket1","ipv6"));
print "socketGetPeerName: "+ str(pycotcp.socketGetPeerName("socket1","ipv6"));
print "socketSend: "+ str(pycotcp.socketSend("socket1","This",100))
print "socketSend: "+ str(pycotcp.socketSend("socket1","is",100))
print "socketSend: "+ str(pycotcp.socketSend("socket1","the",100))
print "socketSend: "+ str(pycotcp.socketSend("socket1","end",100))
try:
    while True:
        pycotcp.stackTick()
        pycotcp.idle()
except KeyboardInterrupt:
    exit
