from ctypes import *
import pycotcp

indirizzo_locale = "10.40.0.50"
mask = "255.255.0.0"
indirizzo_pubblico = indirizzo_locale

porta_pubblico_echo = "5555"
indirizzo_privato_echo = "10.40.0.55"
porta_privato_echo = "6667"

porta_pubblico_client = "5556"
indirizzo_privato_client = "10.40.0.53"
porta_privato_client = "6667"

pycotcp.init()
print "inizializzazione NATBOX fatta"
print "createDevice:" + str(pycotcp.createDevice("vde","pic0","/tmp/pic0.ctl"))
print "linkAddIp4: "+ str(pycotcp.linkAddIp4("pic0",indirizzo_locale,mask))
print "linkGetIp4: "+ str(pycotcp.linkGetIp4("link0",indirizzo_locale))
print "natEnableIp4: "+ str(pycotcp.natEnableIp4("link0"))
print "natEnableIp4: "+ str(pycotcp.portForwardIp4(indirizzo_pubblico,porta_pubblico_echo,indirizzo_privato_echo,porta_privato_echo,"udp","add"))
print "natEnableIp4: "+ str(pycotcp.portForwardIp4(indirizzo_pubblico,porta_pubblico_client,indirizzo_privato_client,porta_privato_client,"udp","add"))
try:
    while True:
        pycotcp.stackTick()
        pycotcp.idle()
except KeyboardInterrupt:
    exit
