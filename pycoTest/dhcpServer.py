from ctypes import *
import pycotcp

pycotcp.init()
print "inizializzazione fatta "
print "Device pic0: " + str(pycotcp.createDevice("vde","pic0","/tmp/pic0.ctl"))
print "linkAddIp4: "+ str(pycotcp.linkAddIp4("pic0","10.40.0.10","255.255.255.0"))
print "dhcpServerInitiate: "+ str(pycotcp.dhcpServerInitiate("pic0","10.40.0.10","255.255.255.0","20","30","120"))
try:
    while True:
        pycotcp.stackTick()
        pycotcp.idle()
except KeyboardInterrupt:
    exit
