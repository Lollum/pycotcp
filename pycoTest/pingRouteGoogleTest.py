from ctypes import *
import pycotcp

localaddr = "10.40.0.5"
localmask = "255.255.255.0"
anyaddr = "0.0.0.0"
gateway = "10.40.0.1"
remoteaddr = "149.3.177.25" #google

def callback_ping(dest,size,seq,time,ttl,err):
    if (err == 0):
        print "ricevuto: "+str(dest)+" size:"+str(size)+" seq:"+str(seq)+" time:"+str(time)+" ttl:"+str(ttl)+" err:"+str(err)
    else:
        print "++ERRORE: "+str(dest)+" seq:"+str(seq)+" err:"+str(err)+"++" 

pycotcp.init()
print "inizializzazione fatta "
print "createDevice:" + str(pycotcp.createDevice("vde","pic0","/tmp/pic0.ctl"))
print "linkAddIp4: "+ str(pycotcp.linkAddIp4("pic0",localaddr,localmask))
print "routeAddIp4: "+ str(pycotcp.routeAddIp4(anyaddr,anyaddr,gateway,1,"null"))
id = pycotcp.pingStartIp4(remoteaddr,10,1000,3000,64,callback_ping)
try:
    while True:
        pycotcp.stackTick()
        pycotcp.idle()
except KeyboardInterrupt:
    exit
