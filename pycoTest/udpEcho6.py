from ctypes import *
import pycotcp

indirizzo_locale = "aaaa::1"
indirizzo_remoto = "aaaa::2"
indirizzo_any = "::0"
mask = "ffff::"
porta_locale = "6667"
porta_remoto = "6667"

def callback_socket(param,socket):
    if (param == "RD"):
        recv = pycotcp.socketRecvFromExt(socket,100,"ipv6")
        print "socketRecv: " + str(recv)
        if (recv[0] == "end"):
            print "socketSendTo " + str(pycotcp.socketSendTo(socket,"dududun",100,indirizzo_remoto,porta_remoto))
        if (recv[0] == "My only friend"):
            print "socketConnect: "+ str(pycotcp.socketConnect(socket,indirizzo_remoto,porta_remoto));
            print "socketWrite " + str(pycotcp.socketWrite(socket,"dududuuun",100))
        print "socketSend: "+ str(pycotcp.socketSend(socket,"[assolo]",100))
    else:
        print "socketEcho: "+str(param)

pycotcp.init()
print "inizializzazione CHITARRA fatta"
print "createDevice:" + str(pycotcp.createDevice("vde","pic0","/tmp/pic0.ctl"))
print "linkAddIp6: "+ str(pycotcp.linkAddIp6("pic0",indirizzo_locale,mask))
print "routingEnableIpv6: "+ str(pycotcp.routingEnableIpv6("pic0"));
print "socketOpen: "+ str(pycotcp.socketOpen("socket1","ipv6","udp",callback_socket))
print "socketBind: "+ str(pycotcp.socketBind("socket1",indirizzo_any,porta_locale));
print "socketGetName: "+ str(pycotcp.socketGetName("socket1","ipv6"));
try:
    while True:
        pycotcp.stackTick()
        pycotcp.idle()
except KeyboardInterrupt:
    exit
