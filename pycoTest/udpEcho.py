from ctypes import *
import pycotcp

indirizzo_locale = "10.40.0.55"
indirizzo_remoto = "10.40.0.50"
mask = "255.255.255.0"
porta_locale = "6667"
porta_remoto = "5556"

def callback_socket(param,socket):
    if (param == "RD"):
        recv = pycotcp.socketRecvFromExt(socket,100,"ipv4")
        print "socketRecvFromExt: " + str(recv)
        if (recv[0] == "end"):
            print "socketSendTo " + str(pycotcp.socketSendTo(socket,"dududun",100,indirizzo_remoto,porta_remoto))
        if (recv[0] == "My only friend"):
            print "socketConnect: "+ str(pycotcp.socketConnect(socket,indirizzo_remoto,porta_remoto));
            print "socketWrite " + str(pycotcp.socketWrite(socket,"dududuuun",100))
        print "socketSend: "+ str(pycotcp.socketSend(socket,"[assolo]",100))
    else:
        print "socketEcho: "+str(param)

pycotcp.init()
print "inizializzazione CHITARRA fatta"
print "createDevice:" + str(pycotcp.createDevice("vde","pic0","/tmp/pic0.ctl"))
print "linkAddIp4: "+ str(pycotcp.linkAddIp4("pic0",indirizzo_locale,mask))
print "socketOpen: "+ str(pycotcp.socketOpen("socket1","ipv4","udp",callback_socket))
print "socketBind: "+ str(pycotcp.socketBind("socket1",indirizzo_locale,porta_locale));
print "socketGetName: "+ str(pycotcp.socketGetName("socket1","ipv4"));
print "socketGetPeerName: "+ str(pycotcp.socketGetPeerName("socket1","ipv4"));
try:
    while True:
        pycotcp.stackTick()
        pycotcp.idle()
except KeyboardInterrupt:
    exit
