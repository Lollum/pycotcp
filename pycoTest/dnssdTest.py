from ctypes import *
import pycotcp
import time

localaddr = "10.40.0.5"
localmask = "255.255.255.0"
anyaddr = "0.0.0.0"
gateway = "10.40.0.1"
ntp = "0.it.pool.ntp.org"
hostname = "host.local";
def callback_register_sd(rtree,buff):
    print "Registrato servizio DNS-SD",buff,rtree

def callback_dnssd_sd(rtree,buff):
    print "Arrivato:",buff,rtree
    print "createKvVector: "+str(pycotcp.createKvVector("vector0"))
    print "dnssdKVVectorAdd: "+str(pycotcp.dnssdKVVectorAdd("vector0","KEY","VALUE"))
    print "dnssdRegisterService: "+str(pycotcp.dnssdRegisterService("WebServer","_http._tcp","80","vector0","30",callback_register_sd))

pycotcp.init()
print "inizializzazione fatta "
print "createDevice:" + str(pycotcp.createDevice("vde","pic0","/tmp/pic0.ctl"))
print "linkAddIp4: "+ str(pycotcp.linkAddIp4("pic0",localaddr,localmask))
print "routeAddIp4: "+ str(pycotcp.routeAddIp4(anyaddr,anyaddr,gateway,1,"null"))
print "dnssdInit: "+ str(pycotcp.dnssdInit(hostname,localaddr,callback_dnssd_sd))

try:
    while True:
        pycotcp.stackTick()
        pycotcp.idle()
except KeyboardInterrupt:
    exit
