from ctypes import *
import pycotcp
import io
localaddr = "10.40.0.3"
remoteaddr = "10.40.0.2"
localmask = "255.255.255.0"
anyaddr = "0.0.0.0"
gateway = "10.40.0.1"
ntp = "0.it.pool.ntp.org"
hostname = "host.local";
filetransfert = 0
filepath = "../testTftp/"
filenameserver = filepath+"canto21.txt"
filenameclient = filepath+"result/Canto21_client.txt"

def callback_tftprxtx(session,event,block,leng,note):
    global filetransfert
    global filenameclient
    if event == pycotcp.TFTP_EV_OPT:
        print "Dimensioni File: "+str(pycotcp.tftpGetOption(session,'file'))
    if event == pycotcp.TFTP_EV_ERR_PEER:
        print "errore remoto"
    if event == pycotcp.TFTP_EV_OK:
        if filetransfert == 0:
            f = io.open(filenameclient, 'w')
        else:
            f = io.open(filenameclient, 'a')
        filetransfert += leng
        print "ricevo: "+str(filetransfert)
        try:
            towrite = block[:leng].decode('utf-8') ##stringa sporca
            f.write(towrite)
        except Exception,e:
            print str(e)
        if leng < 512:
            print "completato"
                        
pycotcp.init()
print "inizializzazione fatta "
print "createDevice:" + str(pycotcp.createDevice("vde","pic0","/tmp/pic0.ctl"))
print "linkAddIp4: "+ str(pycotcp.linkAddIp4("pic0",localaddr,localmask))
result = (pycotcp.tftpSessionSetup("ipv4",filenameserver,remoteaddr))
print "tftpSessionSetup: "+str(result)
print "tftpSetOption: "+str(pycotcp.tftpSetOption(result[0],'file',0))
print "tftpStartRx: "+str(pycotcp.tftpStartRx(pycotcp.TFTP_PORT,filenameserver,callback_tftprxtx,result[0],result[1]))
try:
    while True:
        pycotcp.stackTick()
        pycotcp.idle()
except KeyboardInterrupt:
    exit
