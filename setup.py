from distutils.core import setup, Extension

m_pyco = Extension('pycotcp',
                    include_dirs = ['./include','./pycoHeaders'],
                    libraries = ['vdeplug'],
                    ##sotto era commentato
                    library_dirs = ['./lib'],
                    extra_objects=["./lib/libpicotcp.a"],
                    #extra_link_args=['-static'],
                    sources = ['./pycoSources/pycotcp.c','./pycoSources/pycoutils.c'])                                         
setup(name='pycotcp', version='0.1', ext_modules=[m_pyco])
    
