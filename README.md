# PycoTcp
Per installare il modulo
```sh
$ sudo python setup.py install
```
bisogna modificare il MAKE di picotcp per aggiungere. 
```sh
-fPIC
```
Dalla directory di picotcp, altrimenti manca vde:
```sh
$ make posix core
```
### Configurazione VDE Automatica
##### Per avviare:
```sh
$ ./pycotcp_init.sh start
```
##### Per spegnere:
```sh
$ ./pycotcp_init.sh stop
```
### Configurazione VDE Manuale
##### Per avviare:
```sh
$ sudo vde_switch -s /tmp/pic0.ctl -m 777 -M /tmp/pico.mgmt -d -hub -t vde0
$ sudo /sbin/ifconfig vde0 10.40.0.1 netmask 255.255.0.0 
```
##### Per spegnere:
```sh
$ sudo /sbin/ifconfig vde0 down
$ sudo vdecmd -s /tmp/pico.mgmt shutdown
```
##### Routing per alcuni moduli:
```sh
& echo 1 > /proc/sys/net/ipv4/ip_forward
$ iptables -t nat -A POSTROUTING -o eth0 -j MASQUERADE
$ iptables -A FORWARD -i pic0 -o eth0 -j ACCEPT
$ iptables -A FORWARD -i eth0 -o pic0 -j ACCEPT
```
